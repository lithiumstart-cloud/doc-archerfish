# Archerfish Documentation Repository #



### What is this repository for? ###

This repository is used to create documentation and CAN symbol files. 

### How do I get set up? ###

* Summary of set up
	* BluFlex Software Manual Appendix D covers autogenerating documentation, but is expanded upon below. https://bitbucket.org/lithiumstart-cloud/doc-bluflex/downloads/
	* Install Python 3, may need to uninstall other major versions of Python. https://www.python.org/downloads/
	* Install MikTeX https://miktex.org/download
	* Install GIT which should install Bash. https://git-scm.com/downloads
	* Clone Tools doc gen https://bitbucket.org/lithiumstart-cloud/tools-docgen/src/master/
	* Clone Tool scripts https://bitbucket.org/lithiumstart-cloud/tools-scripts/src/master/ 
	* Clone this repository such that this repo, tools, repo, and tool scrips repo are adjacent to eachother in the same parent folder.
* Configuration
	* icd_config.json tells the script how to pull it together.
	* can.json and all files used are edited manually, is not presently pulled from source code.
	* revisions.json needs to be updated with each change to ICD.
	* acronyms.json lists all acronyms used in ICD.
	* alarms.json lists alarm limits used for temperatures, voltages and current. 
* Dependencies
* Database configuration
* How to generate PDF and Symbol File
	* Right click on the folder that is project specific doc-XXX folder, select GIT Bash Here
	* Upon first run of the below, you may need to uncheck the box for Always show this dialog before installing packages when promted for package installation for latex.
	* -t places a timestamp on the file name and should be kept. -v is verbose console output which is helpful for first time debugging and can be dropped.
	* py ../tools-docgen/json2icd.py -t -v icd_config.json
	* py ../tools-docgen/pcaner.py -tm can.json
* How to run tests
	* Open the PDF and review manually, it will appear after generation in the same folder as this repository.
	* Use PCAN Explorer to open the symbol file, it will check breifly if it is valid, it will appear after generation in the same folder as this repository.
	* Use PCAN Explorer to create a project using the symbol file and a Panel dashboard.
* Deployment instructions
	* Share created files via the Downloads folder

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact